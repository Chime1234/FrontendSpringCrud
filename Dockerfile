FROM node:20.1-alpine3.16

WORKDIR /my-app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install project dependencies
RUN npm install

# Copy the rest of your application code to the container
COPY . .

# Build the application using react-scripts
RUN npm run build

# Start your application
CMD ["npm", "start"]
